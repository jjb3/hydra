FROM mono
ADD Hydra.sln Hydra.sln
ADD Hydra Hydra
RUN nuget restore Hydra.sln
RUN xbuild Hydra.sln