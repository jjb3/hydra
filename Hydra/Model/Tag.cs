﻿using System.ComponentModel.DataAnnotations;

namespace Denxorz.Hydra.Model
{
    public class Tag
    {
        public Tag()
        {
            // Used for EF
        }

        public Tag(string name, Changeset changeset)
        {
            Name = name;
            Changeset = changeset;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public Changeset Changeset { get; set; }
    }
}