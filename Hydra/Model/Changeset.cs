﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Denxorz.Hydra.Model
{
    public class Changeset
    {
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public string RepositoryPath => Repository.Path;

        public int RevisionNumber { get; set; }
        public Repository Repository { get; set; }
        public bool IsClosed { get; set; }
        public DateTime Timestamp { get; set; }
        public string BranchName { get; set; }
        public bool HasTags { get; set; }
        public ICollection<Tag> Tags { get; set; } = new List<Tag>();
        public int LeftParentRevision { get; set; }
        public int RightParentRevision { get; set; }
        public string Hash { get; set; }
        public int? GraftedFromRevision { get; set; }

        public Changeset()
        {
            // Used for EF
        }

        public Changeset(Mercurial.Changeset changeset, Repository repo, bool isClosed, int? graftedFromRevision)
        {
            Repository = repo;
            RevisionNumber = changeset.RevisionNumber;
            Timestamp = changeset.Timestamp;
            BranchName = changeset.Branch;
            HasTags = changeset.Tags.Any();
            Tags = changeset.Tags.Select(t => new Tag(t, this)).ToList();
            LeftParentRevision = changeset.LeftParentRevision;
            RightParentRevision = changeset.RightParentRevision;
            Hash = changeset.Hash;
            IsClosed = isClosed;
            GraftedFromRevision = graftedFromRevision;
        }
    }
}