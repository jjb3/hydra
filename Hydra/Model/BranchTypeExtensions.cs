﻿namespace Denxorz.Hydra.Model
{
    public static class BranchTypeExtensions
    {
        public static BranchType Parse(this BranchType grade, string branchName)
        {
            return GetBranchType(branchName);
        }

        private static BranchType GetBranchType(string name)
        {
            if (name == "default")
            {
                return BranchType.Default;
            }

            if (name == "develop")
            {
                return BranchType.Develop;
            }

            if (name.StartsWith("feature"))
            {
                return BranchType.Feature;
            }

            if (name.StartsWith("release"))
            {
                return BranchType.Release;
            }

            if (name.StartsWith("hotfix"))
            {
                return BranchType.Hotfix;
            }

            if (name.StartsWith("support"))
            {
                return BranchType.Support;
            }

            return BranchType.Other;
        }
    }
}