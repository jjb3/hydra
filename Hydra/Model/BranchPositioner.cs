﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Denxorz.Hydra.Model
{
    public class BranchPositioner
    {
        private const int branchStart = 50;
        private const int branchXOffset = 45;

        public Point this[string name] => BranchLocations[name];
        public Dictionary<string, Point> BranchLocations { get; } = new Dictionary<string, Point>();
        public int DesiredWidth { get; private set; }

        private List<Branch> branchesToPosition;

        public void PositionBranches(IEnumerable<Branch> branches)
        {
            DesiredWidth = branchStart;

            branchesToPosition = branches.OrderBy(b => b, new BranchXComparer()).ToList();
            Position(BranchType.Default);
            Position(BranchType.Hotfix);
            Position(BranchType.Release);
            Position();
        }

        private void Position(BranchType branchType)
        {
            var allBranchesOfType = branchesToPosition.Where(b => b.Type == branchType).ToList();
            foreach (var branch in allBranchesOfType)
            {
                BranchLocations.Add(branch.Name, new Point(DesiredWidth, 0));
                branchesToPosition.Remove(branch);
            }
            DesiredWidth += allBranchesOfType.Any() ? branchXOffset : 0;
        }

        private void Position()
        {
            var lanes = new Dictionary<int, List<Branch>> { { DesiredWidth, new List<Branch>() } };

            foreach (var branch in branchesToPosition)
            {
                KeyValuePair<int, List<Branch>> laneToUse;
                try
                {
                    laneToUse = FirstNonOverlappingLane(lanes, branch);
                }
                catch (InvalidOperationException)
                {
                    laneToUse = CreateLane(lanes);
                }

                laneToUse.Value.Add(branch);
                BranchLocations.Add(branch.Name, new Point(laneToUse.Key, 0));
            }
        }

        private KeyValuePair<int, List<Branch>> FirstNonOverlappingLane(Dictionary<int, List<Branch>> lanes, Branch branch)
        {
            return lanes.First(l => !l.Value.Any(b => b.Overlaps(branch, TimeSpan.FromHours(1))));
        }

        private KeyValuePair<int, List<Branch>> CreateLane(Dictionary<int, List<Branch>> lanes)
        {
            DesiredWidth += branchXOffset;
            var lane = new List<Branch>();
            lanes.Add(DesiredWidth, lane);

            return new KeyValuePair<int, List<Branch>>(DesiredWidth, lane);
        }
    }
}