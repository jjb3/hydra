﻿namespace Denxorz.Hydra.Model
{
    public enum BranchType
    {
        Default,
        Hotfix,
        Release,
        Develop,
        Feature,
        Support,
        Other
    }
}