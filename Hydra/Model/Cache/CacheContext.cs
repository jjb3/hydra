﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Denxorz.Hydra.Model.Cache
{
    public class CacheContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Hydra", "cache.sqlite");
            optionsBuilder.UseSqlite($"Filename={filename}");
        }

        public DbSet<Repository> Repositories { get; set; }
        public DbSet<Changeset> Changesets { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}