﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace Denxorz.Hydra.Model
{
    public class Branch
    {
        public string Name { get; set; }
        public Changeset Start { get; set; }
        public Changeset End { get; set; }
        public List<Changeset> Changesets { get; set; }
        public BranchType Type { get; }

        public Branch(string branchName, IEnumerable<Changeset> changesets)
        {
            Name = branchName;
            Changesets = changesets.ToList();

            Type = BranchType.Other.Parse(Name);
            Start = Changesets.MinBy(r => r.Timestamp);
            End = Changesets.MaxBy(r => r.Timestamp);
        }

        public bool Overlaps(Branch other, TimeSpan requiredSpace)
        {
            return (other.Start.Timestamp >= Start.Timestamp.Add(-requiredSpace) && other.Start.Timestamp <= End.Timestamp.Add(requiredSpace))
                || (other.End.Timestamp >= Start.Timestamp.Add(-requiredSpace) && other.End.Timestamp <= End.Timestamp.Add(requiredSpace))
                || (other.Start.Timestamp < Start.Timestamp.Add(-requiredSpace) && other.End.Timestamp > End.Timestamp.Add(requiredSpace))
                || (other.Start.Timestamp > End.Timestamp.Add(requiredSpace) && !End.IsClosed);
        }
    }
}