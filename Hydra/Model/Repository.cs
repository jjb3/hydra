﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Denxorz.Hydra.Model.Cache;
using Mercurial;

namespace Denxorz.Hydra.Model
{
    public class Repository
    {
        [Key]
        public int Id { get; set; }

        public string Path { get; set; }
        public string Name { get; set; }

        public ICollection<Changeset> Changesets { get; set; } = new List<Changeset>();
        public Changeset this[int revisionNumber] => changesetsPerRevision[revisionNumber];

        [NotMapped]
        private Dictionary<int, Changeset> changesetsPerRevision;

        public Repository()
        {
            // Used for EF
        }

        public Repository(string repoPath)
        {
            Path = repoPath;
            Name = repoPath.Replace(Directory.GetParent(repoPath).FullName, string.Empty).Substring(1);
        }

        public void ReadRepo(BranchPositioner branchPositioner, List<Branch> branches)
        {
            var repo = new Mercurial.Repository(Path);

            var grafts = GetGrafts(repo);

            var tip = GetTip(repo);
            if (Changesets.Any())
            {
                var maxRevisionNumber = Changesets.Max(r => r.RevisionNumber);
                if (tip.RevisionNumber == maxRevisionNumber)
                {
                    changesetsPerRevision = Changesets.ToDictionary(c => c.RevisionNumber, c => c);
                    return;
                }
            }

            var command = new LogCommand().WithTimeout(60 * 10).WithIncludePathActions(false);
            var log = repo.Log(command).ToList();

            var closedCommand = new LogCommand().WithIncludePathActions(false).WithRevision(RevSpec.Closed);
            var closedLog = repo.Log(closedCommand).Select(r => r.RevisionNumber).ToList();

            var changesets = log.Select(r => new Changeset(r, this, closedLog.Contains(r.RevisionNumber), GetGraftedFrom(grafts, r.RevisionNumber))).ToList();

            using (var cache = new CacheContext())
            {
                var changesetsToAdd = changesets.Where(c => !Changesets.Any(c2 => c2.Hash == c.Hash)).ToList();
                changesetsToAdd.ForEach(c => Changesets.Add(c));
                changesetsToAdd.ForEach(c => cache.Changesets.Add(c));
                cache.SaveChanges();
            }
            changesetsPerRevision = Changesets.ToDictionary(c => c.RevisionNumber, c => c);
        }

        private int? GetGraftedFrom(Dictionary<int, int> grafts, int revisionNumber)
        {
            int graftedFrom;
            if (grafts.TryGetValue(revisionNumber, out graftedFrom))
            {
                return graftedFrom;
            }
            return null;
        }

        private static Dictionary<int, int> GetGrafts(Mercurial.Repository repo)
        {
            var getGraftedToCommand = new LogCommand().WithIncludePathActions(false).WithRevision(new RevSpec("destination()"));
            var graftedTo = repo.Log(getGraftedToCommand).ToList();

            var getGraftedFromCommand = new LogCommand().WithIncludePathActions(false).WithRevision(new RevSpec("origin()"));
            var graftedFrom = repo.Log(getGraftedFromCommand).ToList();

            var grafted = graftedTo.ToDictionary(r => r, r => GetSource(r.Hash, repo.Path));

            return graftedTo.Join(graftedFrom, r => grafted[r], r => r.Hash,
                (to, from) => new { To = to.RevisionNumber, From = from.RevisionNumber }).ToDictionary(t => t.To, t => t.From);
        }

        private static Mercurial.Changeset GetTip(Mercurial.Repository repo)
        {
            var lastChangesetCommand = new LogCommand().WithIncludePathActions(false).WithRevision(RevSpec.ByTag("tip"));
            var lastChangesetLog = repo.Log(lastChangesetCommand).ToList();
            return lastChangesetLog.First();
        }

        private static string GetSource(string hash, string repoPath)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = "hg";
                process.StartInfo.Arguments = $"log --debug -r {hash}";
                process.StartInfo.WorkingDirectory = repoPath;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();
                string output = process.StandardOutput.ReadToEnd();

                string pattern = @"extra:\s+source=(?<source>.+)";

                var matches = Regex.Matches(output, pattern, RegexOptions.Compiled | RegexOptions.Multiline);

                if (matches.Count == 0)
                {
                    return null;
                }

                return matches[0].Groups["source"].Value;
            }
        }
    }
}