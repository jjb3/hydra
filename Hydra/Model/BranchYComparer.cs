﻿using System.Collections.Generic;

namespace Denxorz.Hydra.Model
{
    public class BranchXComparer : IComparer<Branch>
    {
        public int Compare(Branch x, Branch y)
        {
            if (x.Type == y.Type || (x.Type > BranchType.Develop && y.Type > BranchType.Develop))
            {
                return x.Start.Timestamp.CompareTo(y.Start.Timestamp);
            }
            return x.Type.CompareTo(y.Type);
        }
    }
}