﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Denxorz.Hydra.Model.Cache;

namespace Denxorz.Hydra.Migrations
{
    [DbContext(typeof(CacheContext))]
    partial class CacheContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("Denxorz.Hydra.Changeset", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BranchName");

                    b.Property<int?>("GraftedFromRevision");

                    b.Property<bool>("HasTags");

                    b.Property<string>("Hash");

                    b.Property<bool>("IsClosed");

                    b.Property<int>("LeftParentRevision");

                    b.Property<int?>("RepositoryId");

                    b.Property<int>("RevisionNumber");

                    b.Property<int>("RightParentRevision");

                    b.Property<DateTime>("Timestamp");

                    b.HasKey("Id");

                    b.HasIndex("RepositoryId");

                    b.ToTable("Changesets");
                });

            modelBuilder.Entity("Denxorz.Hydra.Repository", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Path");

                    b.HasKey("Id");

                    b.ToTable("Repositories");
                });

            modelBuilder.Entity("Denxorz.Hydra.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ChangesetId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("ChangesetId");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("Denxorz.Hydra.Changeset", b =>
                {
                    b.HasOne("Denxorz.Hydra.Repository", "Repository")
                        .WithMany("Changesets")
                        .HasForeignKey("RepositoryId");
                });

            modelBuilder.Entity("Denxorz.Hydra.Tag", b =>
                {
                    b.HasOne("Denxorz.Hydra.Changeset", "Changeset")
                        .WithMany("Tags")
                        .HasForeignKey("ChangesetId");
                });
        }
    }
}
