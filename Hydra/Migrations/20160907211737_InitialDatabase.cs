﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Denxorz.Hydra.Migrations
{
    public partial class InitialDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Repositories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Repositories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Changesets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Autoincrement", true),
                    BranchName = table.Column<string>(nullable: true),
                    HasTags = table.Column<bool>(nullable: false),
                    Hash = table.Column<string>(nullable: true),
                    IsClosed = table.Column<bool>(nullable: false),
                    LeftParentRevision = table.Column<int>(nullable: false),
                    RepositoryId = table.Column<int>(nullable: true),
                    RevisionNumber = table.Column<int>(nullable: false),
                    RightParentRevision = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Changesets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Changesets_Repositories_RepositoryId",
                        column: x => x.RepositoryId,
                        principalTable: "Repositories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Autoincrement", true),
                    ChangesetId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tags_Changesets_ChangesetId",
                        column: x => x.ChangesetId,
                        principalTable: "Changesets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Changesets_RepositoryId",
                table: "Changesets",
                column: "RepositoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_ChangesetId",
                table: "Tags",
                column: "ChangesetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Changesets");

            migrationBuilder.DropTable(
                name: "Repositories");
        }
    }
}
