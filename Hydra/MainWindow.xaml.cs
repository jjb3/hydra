﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Denxorz.Hydra.Model;
using Denxorz.Hydra.Model.Cache;
using Denxorz.Hydra.View;
using Denxorz.Hydra.View.About;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;
using MoreLinq;
using PropertyChanged;

namespace Denxorz.Hydra
{
    [ImplementPropertyChanged]
    public partial class MainWindow
    {
        private readonly Dictionary<string, Dictionary<int, Point>> changesetLocationsPerRevision =
            new Dictionary<string, Dictionary<int, Point>>();

        private readonly Dictionary<string, Dictionary<int, Changeset>> changesetsPerRevision =
            new Dictionary<string, Dictionary<int, Changeset>>();

        private readonly Dictionary<string, Repository> repositories = new Dictionary<string, Repository>();

        private DateTime startTimestamp;
        private DateTime endTimestamp;

        private readonly BranchPositioner branchPositioner;

        public ObservableCollection<Repository> OpenedRepositories { get; } = new ObservableCollection<Repository>();
        public ObservableCollection<BranchVisualizationInfo> Branches { get; } = new ObservableCollection<BranchVisualizationInfo>();
        public ObservableCollection<MergeVisualizationInfo> Merges { get; } = new ObservableCollection<MergeVisualizationInfo>();
        public ObservableCollection<GraftVisualizationInfo> Grafts { get; } = new ObservableCollection<GraftVisualizationInfo>();

        public TimelineVisualizationInfo Timeline { get; private set; } = new TimelineVisualizationInfo();

        public bool ShowTags { get; set; } = true;
        public bool ShowMerges { get; set; } = true;
        public bool ShowChangesets { get; set; } = true;
        public bool ShowBranchNames { get; set; } = true;
        public bool ShowTimeline { get; set; } = true;
        public bool ShowGrafts { get; set; } = true;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            branchPositioner = new BranchPositioner();
        }

        private void OnToolBarLoaded(object sender, RoutedEventArgs e)
        {
            ToolBar toolBar = (ToolBar)sender;
            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }
            var mainPanelBorder = toolBar.Template.FindName("MainPanelBorder", toolBar) as FrameworkElement;
            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness();
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ProgressRing.Visibility = Visibility.Visible;

            using (var cache = new CacheContext())
            {
                cache.Database.Migrate();
            }

            OpenRepository("E:\\Projects\\FlinQ");
            // OpenRepository("E:\\Projects\\FlinQClient");
            // OpenRepository("E:\\Projects\\FlinQServer");
            //OpenRepository("E:\\Projects\\mercurialnet");

            await RedrawAsync();

            ProgressRing.Visibility = Visibility.Hidden;
        }

        private async Task RedrawAsync()
        {
            var changesets = new List<Changeset>();
            var branches = new List<Branch>();
            foreach (var repository in OpenedRepositories)
            {
                await Task.Factory.StartNew(() => repository.ReadRepo(branchPositioner, branches));
                repository.Changesets.ForEach(changesets.Add);
            }

            var orderedLog = changesets.OrderBy(r => r.Timestamp);
            startTimestamp = orderedLog.Min(r => r.Timestamp);
            endTimestamp = orderedLog.Max(r => r.Timestamp);
            var endPixel = (endTimestamp - startTimestamp).TotalDays * BranchVisualizationInfo.pixelsPerDay +
                           BranchVisualizationInfo.topOffset;
            Timeline = new TimelineVisualizationInfo(startTimestamp, endTimestamp, endPixel);

            orderedLog.DistinctBy(r => r.BranchName)
                .Select(r => new Branch(r.BranchName, orderedLog.Where(c => c.BranchName == r.BranchName)))
                .ToList()
                .ForEach(branches.Add);
            branchPositioner.PositionBranches(branches);

            drawingGrid.Width = branchPositioner.DesiredWidth + 100;
            drawingGrid.Height = endPixel + 100;

            orderedLog.DistinctBy(c => c.RepositoryPath)
                .ForEach(c => changesetLocationsPerRevision.Add(c.RepositoryPath, new Dictionary<int, Point>()));

            orderedLog.DistinctBy(c => c.RepositoryPath)
                .ForEach(c => changesetsPerRevision.Add(c.RepositoryPath, new Dictionary<int, Changeset>()));

            branches.Select(
                    b =>
                        new BranchVisualizationInfo(b, endPixel, startTimestamp, branchPositioner,
                            changesetLocationsPerRevision, changesetsPerRevision))
                .ToList()
                .ForEach(Branches.Add);

            Branches.ForEach(b => b.Changesets.ForEach(c => c.GetMerges(Branches).ForEach(Merges.Add)));
            Branches.ForEach(b => b.Changesets.ForEach(c => c.GetGrafts(Branches).ForEach(Grafts.Add)));
        }

        private void OpenRepository(string repoPath)
        {
            Repository repository;
            using (var cache = new CacheContext())
            {
                repository = cache.Repositories
                    .Include(r => r.Changesets)
                    .ThenInclude(c => c.Tags)
                    .FirstOrDefault(r => r.Path == repoPath)
                    ?? new Repository(repoPath);
            }

            repositories.Add(repository.Path, repository);
            OpenedRepositories.Add(repository);
        }

        private void OnAboutButtonClick(object sender, RoutedEventArgs e)
        {
            var about = new AboutWindow { Owner = this };
            about.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog { Filter = "Portable Network Graphics|*.png|All Files|*.*" };
            if (dialog.ShowDialog() == true)
            {
                double dpi = 96d;

                RenderTargetBitmap rtb = new RenderTargetBitmap(
                    (int)drawingGrid.RenderSize.Width,
                    (int)drawingGrid.RenderSize.Height,
                    dpi, dpi, PixelFormats.Default);
                rtb.Render(drawingGrid);

                BitmapEncoder pngEncoder = new PngBitmapEncoder();
                pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

                using (var fs = File.OpenWrite(dialog.FileName))
                {
                    pngEncoder.Save(fs);
                    fs.Close();
                }
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {

        }
    }
}
