﻿using System;
using System.Windows;

namespace Denxorz.Hydra.View
{
    public class TimelineMarkerVisualizationInfo
    {
        public string Label { get; }
        public Point LineStart { get; }

        public TimelineMarkerVisualizationInfo(DateTime startTimestamp, DateTime timestamp)
        {
            Label = timestamp.ToString("MMM yyyy");
            LineStart = new Point(0, BranchVisualizationInfo.topOffset + (timestamp - startTimestamp).TotalDays * BranchVisualizationInfo.pixelsPerDay);
        }
    }
}