﻿using System.Windows;
using Denxorz.Hydra.Model;

namespace Denxorz.Hydra.View
{
    public class TagVisualizationInfo
    {
        public double X { get; }
        public double Y { get; }
        public Point LineStart { get; }
        public Point LineEnd { get; }
        public string Content { get; }

        public TagVisualizationInfo(Tag tag, Point location)
        {
            Content = tag.Name;
            X = 10;
            Y = location.Y + 10;

            //        var x = location.X - 20 + multiOffset;
            //        multiOffset += x;

            // LineStart = new Point(X + label.DesiredSize.Width / 2.0, 20 + label.DesiredSize.Height);
            LineStart = new Point(X, Y);
            LineEnd = location;
        }
    }
}