﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Denxorz.Hydra.Model;

namespace Denxorz.Hydra.View
{
    public class ChangesetVisualizationInfo
    {
        public const int changesetSize = 12;

        private readonly Changeset changeset;
        private readonly Dictionary<string, Dictionary<int, Point>> changesetLocations;
        private readonly Dictionary<string, Dictionary<int, Changeset>> changesetsPerRevision;

        public bool IsClosed { get; }
        public bool IsNormal { get; }
        public double Height { get; }
        public double Width { get; }
        public double X { get; }
        public double Y { get; }

        public List<TagVisualizationInfo> Tags { get; }

        public List<MergeVisualizationInfo> GetMerges(ObservableCollection<BranchVisualizationInfo> branchVisualizationInfo)
        {
            var ret = new List<MergeVisualizationInfo>();
            var branchStartPoint = branchVisualizationInfo.First(b => b.Changesets.Contains(this)).LineStart;
            var startPoint = new Point(branchStartPoint.X + X, branchStartPoint.Y + Y);

            if (changeset.LeftParentRevision >= 0)
            {
                var parentRevision = changesetsPerRevision[changeset.RepositoryPath][changeset.LeftParentRevision];
                if (changeset.BranchName != parentRevision.BranchName)
                {
                    var changesetLocation = changesetLocations[changeset.RepositoryPath][changeset.LeftParentRevision];
                    var branchOfChangeSet = branchVisualizationInfo.First(b => b.Name == parentRevision.BranchName).LineStart;
                    var endPoint = new Point(changesetLocation.X + branchOfChangeSet.X, changesetLocation.Y + branchOfChangeSet.Y);
                    ret.Add(new MergeVisualizationInfo(
                        startPoint,
                        endPoint,
                        Brushes.Gray,
                        0));
                }
            }

            if (changeset.RightParentRevision > 0)
            {
                var parentRevision = changesetsPerRevision[changeset.RepositoryPath][changeset.RightParentRevision];
                var changesetLocation = changesetLocations[changeset.RepositoryPath][changeset.RightParentRevision];
                var branchOfChangeSet = branchVisualizationInfo.First(b => b.Name == parentRevision.BranchName).LineStart;
                var endPoint = new Point(changesetLocation.X + branchOfChangeSet.X, changesetLocation.Y + branchOfChangeSet.Y);
                ret.Add(new MergeVisualizationInfo(
                    startPoint,
                    endPoint,
                    Brushes.LightGray,
                    changesetSize));
            }

            return ret;
        }

        public List<GraftVisualizationInfo> GetGrafts(ObservableCollection<BranchVisualizationInfo> branchVisualizationInfo)
        {
            var ret = new List<GraftVisualizationInfo>();
            var branchStartPoint = branchVisualizationInfo.First(b => b.Changesets.Contains(this)).LineStart;
            var startPoint = new Point(branchStartPoint.X + X, branchStartPoint.Y + Y);

            if (changeset.GraftedFromRevision.HasValue)
            {
                var parentRevision = changesetsPerRevision[changeset.RepositoryPath][changeset.GraftedFromRevision.Value];
                var changesetLocation = changesetLocations[changeset.RepositoryPath][changeset.GraftedFromRevision.Value];

                var branchOfChangeSet = branchVisualizationInfo.First(b => b.Name == parentRevision.BranchName).LineStart;
                var endPoint = new Point(changesetLocation.X + branchOfChangeSet.X, changesetLocation.Y + branchOfChangeSet.Y);
                ret.Add(new GraftVisualizationInfo(
                    startPoint,
                    endPoint,
                    Brushes.LightGray,
                    0));
            }

            return ret;
        }

        public ChangesetVisualizationInfo(Changeset changeset, Branch branch, Dictionary<string, Dictionary<int, Point>> changesetLocations, Dictionary<string, Dictionary<int, Changeset>> changesetsPerRevision)
        {
            this.changeset = changeset;
            this.changesetLocations = changesetLocations;
            this.changesetsPerRevision = changesetsPerRevision;
            IsClosed = changeset.IsClosed;

            if (IsClosed)
            {
                Width = changesetSize + 4;
                Height = changesetSize / 2.0 + 2;
            }
            else
            {
                IsNormal = true;
                Width = changesetSize;
                Height = changesetSize;
            }

            var location = new Point(0, (changeset.Timestamp - branch.Start.Timestamp).TotalDays * BranchVisualizationInfo.pixelsPerDay);
            Y = location.Y;
            X = -changesetSize / (double)2 - (IsClosed ? 2 : 0);

            Tags = changeset.Tags.Select(t => new TagVisualizationInfo(t, location)).ToList();

            changesetLocations[changeset.RepositoryPath].Add(changeset.RevisionNumber, location);
            changesetsPerRevision[changeset.RepositoryPath].Add(changeset.RevisionNumber, changeset);
        }
    }
}