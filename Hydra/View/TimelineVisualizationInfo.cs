﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Denxorz.Hydra.View
{
    public class TimelineVisualizationInfo
    {
        private const int topOffset = 20;

        public Point LineStart { get; }
        public Point LineEnd { get; }

        public List<TimelineMarkerVisualizationInfo> Markers { get; }

        public TimelineVisualizationInfo()
            : this(DateTime.Now, DateTime.Now.AddSeconds(1), 0)
        {
        }

        public TimelineVisualizationInfo(DateTime startTimestamp, DateTime endTimestamp, double endPixel)
        {
            LineStart = new Point(30, topOffset);
            LineEnd = new Point(30, endPixel);

            Markers = new List<TimelineMarkerVisualizationInfo>();

            var startMonth = new DateTime(startTimestamp.Year, startTimestamp.Month, 1);

            var current = startMonth;
            while (current < endTimestamp)
            {
                Markers.Add(new TimelineMarkerVisualizationInfo(startTimestamp, current));
                current = current.AddMonths(1);
            }
        }
    }
}