﻿using System.Windows;
using System.Windows.Media;

namespace Denxorz.Hydra.View
{
    public class GraftVisualizationInfo
    {
        public Brush Brush { get; }
        public Point StartPoint { get; }
        public Point Point1 { get; }
        public Point Point2 { get; }
        public Point Point3 { get; }

        public GraftVisualizationInfo(Point start, Point end, Brush brush, double endOffsetY)
        {
            Brush = brush;

            Point middle = new Point(start.X + (end.X - start.X) / 2.0, start.Y + (end.Y - start.Y) / 2.0);
            StartPoint = start;
            Point1 = new Point(middle.X, start.Y - 5);
            Point2 = new Point(middle.X, end.Y + 5);
            Point3 = new Point(end.X, end.Y + endOffsetY);
        }
    }
}