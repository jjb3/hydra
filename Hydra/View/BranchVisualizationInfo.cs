﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Denxorz.Hydra.Model;

namespace Denxorz.Hydra.View
{
    public class BranchVisualizationInfo
    {
        private readonly Branch branch;
        public const int pixelsPerDay = 40;
        public const int topOffset = 100;

        public Point LineStart { get; }
        public Point LineEnd { get; }
        public Point LabelLocation { get; }
        public Point NonClosedLineEnd { get; }
        public bool HasClosedLine { get; }
        public Brush Brush { get; }
        public string Name { get; }

        public List<ChangesetVisualizationInfo> Changesets { get; }

        public BranchVisualizationInfo(Branch branch, double endPixel, DateTime startTimestamp, BranchPositioner branchLocations, Dictionary<string, Dictionary<int, Point>> changesetLocationsPerRevision, Dictionary<string, Dictionary<int, Changeset>> changesetsPerRevision)
        {
            this.branch = branch;
            var startOffset = branch.Start.Timestamp - startTimestamp;
            var endOffset = branch.End.Timestamp - startTimestamp;

            var x = branchLocations[branch.Name];

            HasClosedLine = !branch.End.IsClosed;
            LineStart = new Point(x.X, startOffset.TotalDays * pixelsPerDay + topOffset);
            LineEnd = new Point(x.X, endOffset.TotalDays * pixelsPerDay + topOffset);
            NonClosedLineEnd = new Point(LineEnd.X, endPixel);
            LabelLocation = new Point(x.X - 30, LineStart.Y - 30);

            Brush = GetBrush();
            Name = branch.Name;

            Changesets = branch.Changesets.Select(c => new ChangesetVisualizationInfo(c, branch, changesetLocationsPerRevision, changesetsPerRevision)).ToList();
        }

        private Brush GetBrush()
        {
            switch (branch.Type)
            {
                case BranchType.Support: return Brushes.SaddleBrown;
                case BranchType.Hotfix: return Brushes.SandyBrown;
                case BranchType.Release: return Brushes.CornflowerBlue;
                case BranchType.Default: return Brushes.LightBlue;
                case BranchType.Develop: return Brushes.MediumPurple;
                case BranchType.Feature: return Brushes.MediumSeaGreen;
                case BranchType.Other: return Brushes.PeachPuff;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}